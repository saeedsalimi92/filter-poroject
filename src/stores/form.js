import {defineStore} from "pinia";


export const useFormStore = defineStore('form', {
    state: () => ({

            formData:[],
            receiveForm:[],
    }
    ),
    getters: {
        getFormData: (state) => state.formData,
        getFilterForm: (state) => {
            return state.receiveForm
        },
    },
    actions: {
        setReceiveForm(payload) {
            this.receiveForm = payload
            this.setForm(this.receiveForm)
        },
        setForm(arr) {
            this.formData = this.convertArray(arr , null , 0)
        },
        onClearAll() {
            this.receiveForm.forEach(item=>{
                if(item.type === 'checkbox-group' || item.type === 'number'){
                    item.value= []
                } else if(item.type === 'checkbox') {
                    item.value = false
                } else {
                    item.value = ''
                }
            })
            this.setForm(this.receiveForm)
            this.setQueryToUrl()

        },
        onClear(payload) {
            const arr = [...this.receiveForm]
            const findIndx = arr.findIndex(form => form.name === payload.name)
            if(arr[findIndx].type === 'checkbox-group' || arr[findIndx].type === 'number'){
                arr[findIndx].value = []
            }else{
                arr[findIndx].value = []
            }
            this.receiveForm = arr
            this.setForm(arr)
            this.setQueryToUrl()
        },
        onClearByName(names) {
            names.forEach(item=>{
                if(item){
                    const find = this.receiveForm.find(form => form.name === item)
                    if(find){
                        this.onClear(find)
                    }
                }
            })

        },
        onChange(item , value) {
            const arr = [...this.receiveForm]
            const findIndx = arr.findIndex(form => form.name === item.name)
            if(findIndx> -1){
                if(arr[findIndx].type === 'checkbox-group'){
                    if(value){
                        if(arr[findIndx].value.includes(value)){
                            const temp = arr[findIndx].value.filter(val=> val !== value)
                            arr[findIndx].value = temp
                        }else{
                            arr[findIndx].value.push(value)
                        }
                    }
                }else{
                    arr[findIndx].value = value
                }
                this.receiveForm = arr
                this.setForm(arr)
                this.setQueryToUrl()
            }

        },
        convertArray(arr, parent = null) {
            const nodes = arr.filter(node => node.parent === parent).map(node => {
                const children = this.convertArray(arr, node.name);
                const props = children.map(child => {
                    return {
                        ...child,
                        props: [],
                    };
                });
                return {
                    children: children.map(child => child.name),
                    props,
                    parent,
                    ...node
                };
            });
            if (parent === null) {
                return [
                    ...nodes,
                ];
            }
            return nodes;
        },
        getDataFromQuery(){

            let query = window.location.search
            let str = query.substring(1).replaceAll('~' , '=').replaceAll('+' , '&')
            const urlSearchParams = new URLSearchParams(str);
            const params = Object.fromEntries(urlSearchParams.entries());
            const arr = Object.entries(params).map(item=>{
                const obj = {}
                obj.name = item[0]
                if(item[1].includes('--')){
                    obj.value = item[1].split('--')
                }else{
                    obj.value = item[1]
                }
                return obj
            })
            this.setQueryToForm(arr)
        },
        setQueryToForm(arr){
            if(arr){
                this.receiveForm.forEach(form=>{
                    arr.forEach(query=>{
                        if(query.name === form.name){
                            if(form.type === 'checkbox-group'){
                                if(typeof query.value === 'string'){
                                    form.value.push(query.value)
                                }else {
                                    form.value = query.value
                                }
                            }else if(form.type === 'checkbox'){
                                if(query.value == 'true'){
                                    form.value = true
                                }else{
                                    form.value = false
                                }

                            }else{
                                form.value = query.value
                            }

                        }
                    })
                })
                this.setForm(this.receiveForm)
            }
        },
        setQueryToUrl(){
            const arr = this.receiveForm.map(item=>{
                const obj = {}
                if(item.value?.length || item.value === true){
                    obj.name = item.name
                    obj.value = item.value
                }
                return obj
            }).filter(item=>item.value)
            const params = new URLSearchParams()
            arr.forEach(item=>{
                params.append(item.name , item.value)
            })
            let str = ''
            if(Array.from(params).length > 0){
                str = '?' +params.toString().replaceAll('=' , '~').replaceAll('&' , '+').replaceAll('%2C' , '--')
            }
            window.history.pushState(null, '', str ? str : '/' );
        }
    },
})
